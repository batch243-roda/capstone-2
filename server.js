require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routes
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);

mongoose.set({ strictQuery: true });
mongoose
  .connect(
    process.env.MONGODB_URL
    // 'mongodb://localhost:27017/capstone2'
  )
  .then(() => {
    app.listen(
      process.env.PORT || 3000,
      console.log('Now connected to MongoDB Atlas'),
      console.log(`Server is running at port ${process.env.PORT || 3000}`)
    );
  })
  .catch((err) => {
    console.log(err);
  });
