const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const productSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    img_url: [{ type: String }],
    details: [
      {
        color: {
          type: String,
          required: true,
        },
        size: {
          type: String,
          required: true,
        },
        stocks: {
          type: Number,
          required: true,
        },
        price: {
          type: Number,
          required: true,
        },
        hasStocks: {
          type: Boolean,
          default: true,
        },
      },
    ],
    categories: [
      {
        main: {
          type: String,
          required: true,
        },
        sub: [
          {
            type: String,
          },
        ],
      },
    ],
    ratings: [
      {
        type: Number,
      },
    ],
    isActive: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model('Product', productSchema);
