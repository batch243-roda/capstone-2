const Product = require('../models/Product');
const mongoose = require('mongoose');
const { decode } = require('../auth');
const cloudinary = require('cloudinary');

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.CLOUD_API_KEY,
  api_secret: process.env.CLOUD_API_SECRET,
});

// Get all active products, all users
const getActiveProducts = async (req, res) => {
  try {
    const name = req.query.name;

    const keys = ['name'];
    const products = await Product.find({ isActive: true }).sort({
      createdAt: -1,
    });

    const search = (data) => {
      return data.filter((item) =>
        keys.some((key) => item[key].toLowerCase().includes(name))
      );
    };

    if (products.length < 1) throw Error('No products available yet!');

    if (name) {
      return res.status(200).json(search(products));
    }

    res.status(200).json(products);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Get specific active product, all users
const getActiveProduct = async (req, res) => {
  const { productId } = req.params;

  if (!mongoose.Types.ObjectId.isValid(productId))
    return res.status(404).json({ error: 'No such product exist!' });

  try {
    const product = await Product.findById(productId);

    if (!product)
      return res.status(404).json({ error: 'No such product exist!' });

    if (!product.isActive)
      return res.status(404).json({ error: 'No such product exist!' });

    res.status(200).json(product);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Get all products active/inactive, admin only
const getProducts = async (req, res) => {
  const token = req.headers.authorization;
  const userData = decode(token);

  if (!userData.isAdmin)
    return res.status(401).json({ error: 'Access Denied!, Admin users only!' });

  try {
    const product = await Product.find();

    if (product.length < 1)
      return res.status(400).json({ error: 'No products available yet!' });

    res.status(200).json(product);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Create product, admin only
const createProduct = async (req, res) => {
  const { name, description, img_url, color, size, stocks, price, main, sub } =
    req.body;

  const token = req.headers.authorization;
  const userData = decode(token);

  if (!userData.isAdmin)
    return res.status(401).json({ error: 'Access Denied!, Admin users only!' });

  try {
    const sameProductName = await Product.find({ name });

    if (sameProductName.length > 0) {
      let arrSize = sameProductName[0].details.length;
      for (let i = 0; i < arrSize; i++) {
        if (sameProductName[0].details[i].color === color) {
          if (sameProductName[0].details[i].size === size) {
            return res.status(400).json({
              error: `Product ${name} already existed with this size: ${size}, and color: ${color}`,
            });
          } else {
            if (i === arrSize - 1) {
              sameProductName[0].details.push({ color, size, stocks, price });
            }
          }
        } else {
          if (i === arrSize - 1) {
            sameProductName[0].img_url.push(img_url);
            sameProductName[0].details.push({ color, size, stocks, price });
          }
        }
      }
      sameProductName[0].save();
      return res.status(200).json(sameProductName);
    }

    const product = await Product.create({
      name,
      description,
      img_url,
      details: [
        {
          color,
          size,
          stocks,
          price,
        },
      ],
      categories: [
        {
          main,
          sub,
        },
      ],
    });
    res.status(200).json(product);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Update product, admin only
const updateProduct = async (req, res) => {
  const {
    productId,
    name,
    description,
    detailsId,
    color,
    size,
    stocks,
    price,
    img_url,
  } = req.body;

  const token = req.headers.authorization;
  const userData = decode(token);

  // Validate if product ID inputted is same as mongoDB format
  if (!mongoose.Types.ObjectId.isValid(productId))
    return res.status(404).json({ error: 'No such product exist!' });

  try {
    if (!userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Admin users only!' });

    const product = await Product.findById(productId);

    if (!product) {
      return res.status(404).json({ error: `No such product exist!` });
    }

    if (!product.isActive && product.stocks !== 0) {
      productIsActive = !product.isActive;
    }

    if (req.body.stocks === 0) {
      return res.status(400).json('You cannot set stocks to 0');
    }

    if (product.stocks === 0 && req.body.stocks === 0) {
      return res.status(400).json({
        error:
          'You cannot update a product that has 0 stocks, please add stocks',
      });
    }

    for (let i = 0; i < product.details.length; i++) {
      if (product.details[i]._id.toString() === detailsId) {
        if (color) product.details[i].color = color;
        if (size) product.details[i].size = size;
        if (stocks) product.details[i].stocks = stocks;
        if (price) product.details[i].price = price;
        if (img_url) product.img_url = img_url;
        if (name) product.name = name;
        if (description) product.description = description;
      }
    }

    await product.save();

    res.status(200).json(product);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Archived the product, admin only
const archivedProduct = async (req, res) => {
  const { productId } = req.body;

  const token = req.headers.authorization;
  const userData = decode(token);

  // Validate if product ID inputted is same as mongoDB format
  if (!mongoose.Types.ObjectId.isValid(productId))
    return res.status(400).json({ error: 'No such product exist' });

  try {
    if (!userData.isAdmin)
      return res.status(401).json('Access Denied!, Admins users only!');

    const product = await Product.findById(productId);

    if (!product) {
      return res.status(400).json({ error: 'No such product exist' });
    }

    if (product.stocks === 0) {
      return res
        .status(400)
        .json('You cannot active a product that has 0 stocks');
    }

    const productIsActive = !product.isActive;

    const updatedProduct = await Product.findByIdAndUpdate(
      productId,
      { isActive: productIsActive },
      { new: true }
    );

    if (!updatedProduct) {
      return res.status(400).json({ error: 'No such product exist' });
    }

    res.status(200).json(updatedProduct);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

const deleteProductImage = async (req, res) => {
  const { public_id } = req.body;

  try {
    const response = await cloudinary.v2.uploader.destroy(public_id);
    res.status(200).json(response);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

module.exports = {
  getActiveProducts,
  getActiveProduct,
  getProducts,
  createProduct,
  updateProduct,
  archivedProduct,
  deleteProductImage,
};
