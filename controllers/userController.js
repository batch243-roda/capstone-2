const User = require('../models/User');
const Product = require('../models/Product');
const Cart = require('../models/Cart');
const Order = require('../models/Order');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const { createToken, decode } = require('../auth');

// User register
const signupUser = async (req, res) => {
  const {
    firstName,
    lastName,
    cellNumber,
    gender,
    street,
    barangay,
    municipality,
    province,
    region,
    username,
    email,
    password,
    confirmPassword,
  } = req.body;

  try {
    const user = await User.signup(
      firstName,
      lastName,
      cellNumber,
      gender,
      street,
      barangay,
      municipality,
      province,
      region,
      username,
      email,
      password,
      confirmPassword
    );
    // create token
    const token = createToken(user);

    res.status(200).json(token);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// User login
const login = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.login(email, password);
    // create token
    const token = createToken(user);
    res.status(200).json(token);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

const getUserProfile = async (req, res) => {
  const token = req.headers.authorization;
  const userData = decode(token);

  try {
    const user = await User.findById(userData.id, { password: false });

    if (!user) return res.status(404).json({ error: 'No such user exist!' });

    res.status(200).json(user);
  } catch (error) {
    res.status(400).json(error);
  }
};

// update role, admin only!
const updateRole = async (req, res) => {
  const token = req.headers.authorization;
  const userData = decode(token);

  const { userId } = req.params;

  // Validate if user ID inputted is same as mongoDB format
  if (!mongoose.Types.ObjectId.isValid(userId))
    return res.status(400).json({ error: 'No such user exist!' });

  try {
    if (!userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Admin users only!' });

    const user = await User.findById(userId);

    if (user.length < 1)
      return res.status(404).json({ error: 'No such user exist!' });

    const userUpdated = await User.findByIdAndUpdate(
      userId,
      { isAdmin: !user.isAdmin },
      { new: true }
    );

    return res.status(200).json(userUpdated);
  } catch (error) {
    return res.status(400).json({ error: error.message });
  }
};

// Add to cart, regular users only!
const addToCart = async (req, res) => {
  let { productId, quantity, color, size } = req.body;

  if (!quantity) {
    quantity = 1;
  }

  const token = req.headers.authorization;
  const userData = decode(token);

  // Validate if user ID inputted is same as mongoDB format
  if (!mongoose.Types.ObjectId.isValid(productId))
    return res.status(404).json({ error: 'No such product exist!' });

  try {
    if (userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Regular users only!' });

    const product = await Product.findById(productId);

    if (!product)
      return res.status(404).json({ error: 'No such product exist!' });

    if (!product.isActive)
      return res
        .status(400)
        .json('This product is already inactive sorry for inconvenience');

    let detailsLength = product.details.length;
    let price = 0;

    for (let i = 0; i < detailsLength; i++) {
      if (product.details[i].size === size) {
        if (product.details[i].color === color) {
          price = product.details[i].price;
          if (!product.details[i].hasStocks) {
            return res.status(400).json({
              error: `Sorry, No more stocks on this item ${product.name} size: ${product.details[i].size}`,
            });
          }

          if (product.details[i].stocks < quantity) {
            return res.status(400).json({
              error: `Stocks insufficient!, remaining stocks ${product.details[i].stocks}`,
            });
          }
          break;
        }
      }
    }

    // Destructuring the product model
    const { name } = product;

    const subTotal = quantity * price;

    // I need to find if the user already has this item in the cart
    const existingCart = await Cart.find({
      userId: userData.id,
    });

    if (existingCart.length > 0) {
      for (let i = 0; i < existingCart[0].products.length; i++) {
        if (
          existingCart[0].products[i].productId === productId &&
          existingCart[0].products[i].color === color &&
          existingCart[0].products[i].size === size
        ) {
          existingCart[0].products[i].quantity += quantity;
          existingCart[0].products[i].subTotal += subTotal;
          await existingCart[0].save();
          return res.status(200).json(existingCart);
        }
      }
      existingCart[0].products.push({
        productId,
        name,
        color,
        size,
        price,
        quantity,
        subTotal,
      });
      await existingCart[0].save();
      return res.status(200).json(existingCart);
    }

    const cart = await Cart.create({
      userId: userData.id,
      products: {
        productId,
        name,
        color,
        size,
        quantity,
        price,
        subTotal,
      },
    });

    res.status(200).json(cart);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Remove to cart, regular users only
const removeFromCart = async (req, res) => {
  const { cartProductId } = req.body;

  const token = req.headers.authorization;
  const userData = decode(token);

  // Validate if user ID inputted is same as mongoDB format
  if (!mongoose.Types.ObjectId.isValid(cartProductId))
    return res.status(404).json({ error: 'No such product exist!' });

  try {
    if (userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Regular users only!' });
    const cart = await Cart.find({
      userId: userData.id,
    });

    if (cart.length < 1) {
      return res.status(404).json({ error: 'No such cart exist!' });
    }

    const productSize = cart[0].products.length;

    for (let i = 0; i < productSize; i++) {
      if (cart[0].products[i]._id.toString() === cartProductId) {
        res
          .status(200)
          .json(
            `${cart[0].products[i].name} color: ${cart[0].products[i].color}, size ${cart[0].products[i].size} is deleted succesfully from cart`
          );
        cart[0].products.splice(i, 1);
        cart[0].save();

        break;
      }
    }
  } catch (error) {
    return res.status(400).json({ error: error.message });
  }
};

// Show cart, regular users only
const showCart = async (req, res) => {
  // const { userId } = req.params;

  const token = req.headers.authorization;
  const userData = decode(token);

  try {
    if (userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Regular users only!' });

    const cart = await Cart.find(
      { userId: userData.id },
      { createdAt: false, updatedAt: false, __v: false }
    );

    if (cart.length < 1)
      return res.status(404).json(`No products in your cart yet!`);

    res.status(200).json(cart);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Checkout, regular users only!
const checkout = async (req, res) => {
  const token = req.headers.authorization;
  const userData = decode(token);

  try {
    if (userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Regular users only!' });

    // Finding the cart that has same userId if multiple it will return array of objects
    const carts = await Cart.find({ userId: userData.id });

    if (carts.length < 1)
      return res.status(404).json('No such cart exist on this user!');

    // Temporary container products array and totalAmount for Order model
    const products = [];
    let totalAmount = 0;

    // Temporary container, product ids for Product model
    const productIds = [];

    // Loop to sum totalamount on each item of cart and push the products id and quantity in temporary container
    // i need to seperate the productIds for finding the Product Model
    for (let i = 0; i < carts.length; i++) {
      for (let j = 0; j < carts[i].products.length; j++) {
        totalAmount += carts[i].products[j].subTotal;
        products.push({
          img_url: carts[i].img_url,
          productId: carts[i].products[j].productId,
          name: carts[i].products[j].name,
          color: carts[i].products[j].color,
          size: carts[i].products[j].size,
          price: carts[i].products[j].price,
          quantity: carts[i].products[j].quantity,
          subTotal: carts[i].products[j].subTotal,
        });
        productIds.push(carts[i].products[j].productId);
      }
    }

    // Find the products and update it using save().
    const updatedProducts = await Product.find({
      _id: productIds,
    });

    if (updatedProducts.length < 1)
      return res.status(404).json({ error: 'No such product exist!' });

    for (let i = 0; i < updatedProducts.length; i++) {
      let detailsLength = updatedProducts[i].details.length;

      for (let j = 0; j < detailsLength; j++) {
        for (let k = 0; k < products.length; k++) {
          if (
            updatedProducts[i].name === products[k].name &&
            updatedProducts[i].details[j].size === products[k].size
          ) {
            if (updatedProducts[i].details[j].color === products[k].color) {
              if (!updatedProducts[i].details[j].hasStocks) {
                return res.status(400).json({
                  error: `Sorry, No more stocks on this item ${updatedProducts.name} size: ${updatedProducts.details[i].size}`,
                });
              }
              if (updatedProducts[i].details[j].stocks < products[k].quantity) {
                return res.status(400).json({
                  error: `Stocks insufficient!, remaining stocks ${updatedProducts[i].details[j].stocks}`,
                });
              }

              updatedProducts[i].details[j].stocks -= products[k].quantity;

              if (updatedProducts[i].details[j].stocks === 0) {
                updatedProducts[i].details[j].hasStocks = false;
              }
              break;
            }
          }
        }
      }
      await updatedProducts[i].save();
    }

    // Create the order/receipt
    const order = await Order.create({
      userId: userData.id,
      products,
      totalAmount,
    });

    // delete the cart after checking out
    await Cart.deleteMany({ userId: userData.id });

    res.status(200).json(order);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Buy now, regular users only
const buyNow = async (req, res) => {
  let { productId, quantity, color, size } = req.body;

  if (!quantity) {
    quantity = 1;
  }

  const token = req.headers.authorization;
  const userData = decode(token);

  // Validate if user ID inputted is same as mongoDB format
  if (!mongoose.Types.ObjectId.isValid(productId))
    return res.status(404).json({ error: 'No such product exist!' });

  try {
    if (userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Regular users only!' });

    const product = await Product.findById(productId);

    if (product.length < 1)
      return res.status(404).json({ error: 'No such product exist' });

    let detailsLength = product.details.length;
    let price = 0;

    for (let i = 0; i < detailsLength; i++) {
      if (product.details[i].size === size) {
        if (product.details[i].color === color) {
          if (!product.details[i].hasStocks) {
            return res.status(400).json({
              error: `Sorry, No more stocks on this item ${product.name} size: ${product.details[i].size}`,
            });
          }

          if (product.details[i].stocks < quantity) {
            return res.status(400).json({
              error: `Stocks insufficient!, remaining stocks ${product.details[i].stocks}`,
            });
          }

          product.details[i].stocks -= quantity;
          price = product.details[i].price;

          if (product.details[i].stocks === 0) {
            product.details[i].hasStocks = false;
          }

          await product.save();
          break;
        }
      }
    }
    const { name } = product;

    const totalAmount = quantity * price;
    const order = await Order.create({
      userId: userData.id,
      products: {
        productId,
        name,
        color,
        size,
        price,
        quantity,
        subTotal: totalAmount,
      },
      totalAmount,
    });

    res.status(200).json(order);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Show  order user only
const showOrder = async (req, res) => {
  const token = req.headers.authorization;
  const userData = decode(token);

  try {
    if (userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Regular users only!' });

    const order = await Order.find({ userId: userData.id });
    if (order.length < 1)
      return res.status(404).json({ error: 'No orders available yet!' });
    return res.status(200).json(order);
  } catch (error) {
    return res.status(400).json({ error: error.message });
  }
};

// Show order admin only
const showAllOrder = async (req, res) => {
  const token = req.headers.authorization;
  const userData = decode(token);

  try {
    if (!userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Admin users only!' });

    const order = await Order.find({});
    if (order.length < 1)
      return res.status(404).json({ error: 'No orders available yet!' });

    res.status(200).json(order);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

const showPendingOrder = async (req, res) => {
  const token = req.headers.authorization;
  const userData = decode(token);

  try {
    if (!userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Admin users only!' });

    const order = await Order.find({ status: 'pending' });
    if (order.length < 1)
      return res
        .status(404)
        .json({ error: "No 'pending' order available yet!" });

    res.status(200).json(order);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

const showApprovedOrder = async (req, res) => {
  const token = req.headers.authorization;
  const userData = decode(token);

  try {
    if (!userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Admin users only!' });

    const order = await Order.find({ status: 'approved' });
    if (order.length < 1)
      return res
        .status(404)
        .json({ error: "No 'approved' order available yet!" });

    res.status(200).json(order);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

const showCancelledOrder = async (req, res) => {
  const token = req.headers.authorization;
  const userData = decode(token);

  try {
    if (!userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Admin users only!' });

    const order = await Order.find({ status: 'cancelled' });
    if (order.length < 1)
      return res
        .status(404)
        .json({ error: "No 'cancelled' order available yet!" });

    res.status(200).json(order);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Approve Order, Admin only
const approvedOrder = async (req, res) => {
  const { orderId } = req.body;

  const token = req.headers.authorization;
  const userData = decode(token);

  try {
    if (!userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Admin users only!' });

    const order = await Order.findById(orderId);
    if (!order) return res.status(404).json({ error: 'No such Order exist!' });

    if (order.status === 'approved')
      return res.status(400).json({ error: 'This order already approved' });

    order.status = 'approved';
    await order.save();
    res.status(200).json(order);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Cancel Order, Regular only
const cancelOrder = async (req, res) => {
  const { orderId } = req.body;

  const token = req.headers.authorization;
  const userData = decode(token);

  try {
    if (userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Regular users only!' });

    const order = await Order.findById(orderId);
    if (order.length < 1)
      return res.status(404).json({ error: 'No such order exist!' });

    if (order.status === 'approved')
      return res.status(400).json({
        error: 'This order already approved, cancellation of order denied!',
      });

    const products = [];
    const productIds = [];

    for (let i = 0; i < order.products.length; i++) {
      products.push({
        name: order.products[i].name,
        color: order.products[i].color,
        size: order.products[i].size,
        quantity: order.products[i].quantity,
      });
      productIds.push(order.products[i].productId);
    }

    const updatedProducts = await Product.find({ _id: productIds });

    for (let i = 0; i < updatedProducts.length; i++) {
      let detailsLength = updatedProducts[i].details.length;

      for (let j = 0; j < detailsLength; j++) {
        for (let k = 0; k < products.length; k++) {
          if (
            updatedProducts[i].name === products[k].name &&
            updatedProducts[i].details[j].size === products[k].size
          ) {
            if (updatedProducts[i].details[j].color === products[k].color) {
              if (!updatedProducts[i].details[j].hasStocks) {
                return res.status(400).json({
                  error: `Sorry, No more stocks on this item ${updatedProducts.name} size: ${updatedProducts.details[i].size}`,
                });
              }

              if (updatedProducts[i].details[j].stocks < products[k].quantity) {
                return res.status(400).json({
                  error: `Stocks insufficient!, remaining stocks ${updatedProducts[i].details[j].stocks}`,
                });
              }

              if (updatedProducts[i].details[j].stocks === 0) {
                updatedProducts[i].details[j].hasStocks = true;
              }

              updatedProducts[i].details[j].stocks += products[k].quantity;
              break;
            }
          }
        }
      }
      await updatedProducts[i].save();
    }

    order.status = 'cancelled';
    await order.save();

    res.status(200).json(order);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Receive Order, Regular users only
const receiveOrder = async (req, res) => {
  const { orderId } = req.body;

  const token = req.headers.authorization;
  const userData = decode(token);

  try {
    if (userData.isAdmin)
      return res
        .status(401)
        .json({ error: 'Access Denied!, Regular users only!' });

    const order = await Order.findById(orderId);
    if (order.length < 1)
      return res.status(404).json({ error: 'No such order exist!' });

    if (order.status === 'pending')
      return res.status(400).json({ error: 'This order is still pending!' });

    if (order.orderReceived)
      return res.status(400).json({ error: 'Order already received!' });
    order.orderReceived = true;
    await order.save();
    res.status(200).json(order);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

module.exports = {
  signupUser,
  login,
  updateRole,
  addToCart,
  removeFromCart,
  showCart,
  checkout,
  buyNow,
  showOrder,
  showAllOrder,
  showPendingOrder,
  showApprovedOrder,
  showCancelledOrder,
  approvedOrder,
  cancelOrder,
  receiveOrder,
  getUserProfile,
};
