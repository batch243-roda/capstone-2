const express = require('express');

const router = express.Router();

const {
  getActiveProducts,
  getActiveProduct,
  getProducts,
  createProduct,
  updateProduct,
  archivedProduct,
  deleteProductImage,
} = require('../controllers/productController');

const { verify } = require('../auth');

// Get all active products, all users
router.get('/', getActiveProducts);

// Get all products active/inactive, admin only
router.get('/all', verify, getProducts);

// Create product, admin only
router.post('/create', verify, createProduct);

// Update product, admin only
router.put('/update', verify, updateProduct);

// Archived the product, admin only
router.patch('/archived', verify, archivedProduct);

router.post('/removeImage', deleteProductImage);

// Get specific active product, all users
router.get('/:productId', getActiveProduct);

module.exports = router;
