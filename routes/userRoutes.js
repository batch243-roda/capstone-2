const express = require('express');
const router = express.Router();

const {
  signupUser,
  login,
  updateRole,
  addToCart,
  showCart,
  removeFromCart,
  checkout,
  buyNow,
  showOrder,
  showAllOrder,
  showPendingOrder,
  showApprovedOrder,
  showCancelledOrder,
  approvedOrder,
  receiveOrder,
  cancelOrder,
  getUserProfile,
} = require('../controllers/userController');

const { verify } = require('../auth');

// User register
router.post('/login', login);

// User login
router.post('/signup', signupUser);

// Show User Profile
router.get('/profile', verify, getUserProfile);

// Show cart, regular users only
router.get('/showCart', verify, showCart);

// Buy now, regular users only
router.post('/buyNow', verify, buyNow);

// Show order regular user only
router.get('/orders', verify, showOrder);

router.get('/showAllOrders', verify, showAllOrder);
router.get('/showPendingOrders', verify, showPendingOrder);
router.get('/showApprovedOrders', verify, showApprovedOrder);
router.get('/showCancelledOrders', verify, showCancelledOrder);

router.post('/approvedOrder', verify, approvedOrder);

router.post('/cancel', verify, cancelOrder);

// Checkout, regular users only!
router.post('/checkout', verify, checkout);

// Add to cart, regular users only!
router.post('/addToCart', verify, addToCart);

// Remove to cart, regular users only
router.delete('/removeFromCart', verify, removeFromCart);

// Received Order
router.post('/orderReceived', verify, receiveOrder);

// update role, admin only!
router.patch('/:userId/updateRole', verify, updateRole);

module.exports = router;
